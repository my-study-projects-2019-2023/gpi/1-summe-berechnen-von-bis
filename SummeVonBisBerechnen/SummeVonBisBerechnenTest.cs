﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace UnitTestProject1
{
    [TestClass]
    public class SummeVonBisBerechnenTest
    {
        private const int startZahl = 7;
        private const int endZahl = 64;
        private const int expected = 2059;

        [TestMethod]
        public void Summe_von_7_bis_64_mit_for()
        {
            int result = BerechneSummeVonBisMitFor(startZahl, endZahl);
            result.Should().Be(expected);
        }

        [TestMethod]
        public void Summe_von_7_bis_64_mit_while()
        {
            int result = BerechneSummeVonBisMitWhile(startZahl, endZahl);
            result.Should().Be(expected);
        }

        [TestMethod]
        public void Summe_von_7_bis_64_mit_doWhile()
        {
            int result = BerechneSummeVonBisMitDoWhile(startZahl, endZahl);
            result.Should().Be(expected);
        }

        [TestMethod]
        public void Summe_von_7_bis_64_mit_rekursiv()
        {
            int result = BerechneSummeVonBisMitRekursion(startZahl, endZahl, 0);
            result.Should().Be(expected);
        }

        private int BerechneSummeVonBisMitRekursion(int startZahl, int endZahl, int summe)
        {
            if (startZahl <= endZahl)
            {
                summe += startZahl;
               return BerechneSummeVonBisMitRekursion((++startZahl), endZahl, summe);
            } else { return summe; }
        }

        private int BerechneSummeVonBisMitDoWhile(int startZahl, int endZahl)
        {
            int summe = 0;
            do
            {
                summe += startZahl;
                startZahl++;
            } while(startZahl <= endZahl);
            return summe;
        }

        private int BerechneSummeVonBisMitWhile(int startZahl, int endZahl)
        {
            int summe = 0;
            while(startZahl <= endZahl)
            {
                summe += startZahl;
                startZahl++;
            }
            return summe;
        }

        private int BerechneSummeVonBisMitFor(int startZahl, int endZahl)
        {
            int summe = 0;
            for(int i = startZahl; i <= endZahl; i++)
            {
                summe += i;
            }
            return summe;
        }
    }
}
